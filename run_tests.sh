#!/bin/bash

rm -r target/test-classes
# for some reason it acts very weirdly when test files are updated
# so I googled that removing test-classes folder helps. it does.
# what a wonderful tooling, I'm glad there are languages without
# such tomfoolery
./mvnw test
