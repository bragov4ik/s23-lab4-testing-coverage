import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;

public class ForumDAOTests {
    void UserListGenericTest(String expectedQuery, String slug, Number limit, String since, Boolean desc) {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ForumDAO.UserList(slug, limit, since, desc);
        verify(mockJdbc).query(Mockito.eq(expectedQuery),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("User gets list of users test #1")
    void UserListTest1() {
        UserListGenericTest(
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;",
                "slug", null, null,
                false);
    }

    @Test
    @DisplayName("User gets list of users test #2")
    void UserListTest2() {
        UserListGenericTest(
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;",
                "slug", 10, "12.02.2019",
                false);
    }

    @Test
    @DisplayName("User gets list of users test #3")
    void UserListTest3() {
        UserListGenericTest(
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;",
                "slug", null, "12.02.2019",
                true);
    }
}
