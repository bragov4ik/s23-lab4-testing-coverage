import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.sql.Timestamp;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;

public class PostDAOTests {
    private Timestamp sampleTimestamp = new Timestamp(0);
    private Integer parent = 1337;
    private Integer thread = 228;
    private Post samplePost = new Post("a", sampleTimestamp, "f", "m", parent, thread, false);

    // CC = 8, so 8 cases are needed
    @Test
    @DisplayName("User gets list of users test #1")
    void setPostTest1() {
        Integer id = 0;
        Post post = new Post();
        post.setAuthor(null);
        post.setMessage(null);
        post.setCreated(null);

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Mockito.when(
                mockJdbc.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
                .thenReturn(samplePost);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.setPost(id, post);
        verify(mockJdbc, never()).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"));
    }

    @Test
    @DisplayName("User gets list of users test #3")
    void setPostTest3() {
        Integer id = 0;
        Timestamp ts = new Timestamp(123);
        Post post = new Post();
        post.setAuthor(null);
        post.setMessage("message");
        post.setCreated(ts);

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Mockito.when(
                mockJdbc.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
                .thenReturn(samplePost);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.setPost(id, post);
        verify(mockJdbc, times(1)).update(
                Mockito.eq(
                        "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq("message"),
                Mockito.eq(ts),
                Mockito.eq(id));
    }

    @Test
    @DisplayName("User gets list of users test #4")
    void setPostTest4() {
        Integer id = 0;
        Timestamp ts = new Timestamp(123);
        Post post = new Post();
        post.setAuthor("author");
        post.setMessage(null);
        post.setCreated(ts);

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Mockito.when(
                mockJdbc.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
                .thenReturn(samplePost);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.setPost(id, post);
        verify(mockJdbc, times(1)).update(
                Mockito.eq(
                        "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq("author"),
                Mockito.eq(ts),
                Mockito.eq(id));
    }

    @Test
    @DisplayName("User gets list of users test #5")
    void setPostTest5() {
        Integer id = 0;
        Timestamp ts = new Timestamp(123);
        Post post = new Post();
        post.setAuthor("author");
        post.setMessage("message");
        post.setCreated(null);

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Mockito.when(
                mockJdbc.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
                .thenReturn(samplePost);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.setPost(id, post);
        verify(mockJdbc, times(1)).update(
                Mockito.eq(
                        "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                Mockito.eq("author"),
                Mockito.eq("message"),
                Mockito.eq(id));
    }

    @Test
    @DisplayName("User gets list of users test #6")
    void setPostTest6() {
        Integer id = 0;
        Timestamp ts = new Timestamp(123);
        Post post = new Post();
        post.setAuthor(null);
        post.setMessage(null);
        post.setCreated(ts);

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Mockito.when(
                mockJdbc.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
                .thenReturn(samplePost);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.setPost(id, post);
        verify(mockJdbc, times(1)).update(
                Mockito.eq(
                        "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(ts),
                Mockito.eq(id));
    }

    @Test
    @DisplayName("User gets list of users test #7")
    void setPostTest7() {
        Integer id = 0;
        Timestamp ts = new Timestamp(123);
        Post post = new Post();
        post.setAuthor("author");
        post.setMessage(null);
        post.setCreated(null);

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Mockito.when(
                mockJdbc.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
                .thenReturn(samplePost);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.setPost(id, post);
        verify(mockJdbc, times(1)).update(
                Mockito.eq(
                        "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                Mockito.eq("author"),
                Mockito.eq(id));
    }

    @Test
    @DisplayName("User gets list of users test #8")
    void setPostTest8() {
        Integer id = 0;
        Timestamp ts = new Timestamp(123);
        Post post = new Post();
        post.setAuthor(null);
        post.setMessage("message");
        post.setCreated(null);

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Mockito.when(
                mockJdbc.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
                .thenReturn(samplePost);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.setPost(id, post);
        verify(mockJdbc, times(1)).update(
                Mockito.eq(
                        "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Mockito.eq("message"),
                Mockito.eq(id));
    }
}
