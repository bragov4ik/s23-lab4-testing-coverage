import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;

public class ThreadDAOTests {
    @Test
    @DisplayName("Tree sort test #1")
    void treeSortTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ThreadDAO thread = new ThreadDAO(mockJdbc);
        ThreadDAO.treeSort(1, 2, 3, true);
        verify(mockJdbc).query(Mockito.eq(
                "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.anyInt(),
                Mockito.anyInt(),
                Mockito.anyInt());
    }

    @Test
    @DisplayName("Tree sort test #2")
    void treeSortTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ThreadDAO thread = new ThreadDAO(mockJdbc);
        ThreadDAO.treeSort(1, 2, 3, false);
        verify(mockJdbc).query(Mockito.eq(
                "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.anyInt(),
                Mockito.anyInt(),
                Mockito.anyInt());
    }
}