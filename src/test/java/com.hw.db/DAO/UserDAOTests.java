import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.verification.VerificationMode;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;

public class UserDAOTests {
    void ChangeGenericTest(String expectedQuery, User user, boolean expectedCall) {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        UserDAO.Change(user);

        VerificationMode verificationMode = never();
        if (expectedCall) {
            verificationMode = times(1);
        }
        verify(mockJdbc, verificationMode).update(Mockito.eq(expectedQuery));
    }

    @Test
    @DisplayName("User gets list of users test #1")
    void ChangeTest1() {
        User user = new User();
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc, never()).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"));
    }

    @Test
    @DisplayName("User gets list of users test #2")
    void ChangeTest2() {
        User user = new User("a", "b", "c", "d");

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc, times(1)).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(user.getEmail()),
                Mockito.eq(user.getFullname()),
                Mockito.eq(user.getAbout()),
                Mockito.eq(user.getNickname()));
    }

    @Test
    @DisplayName("User gets list of users test #3")
    void ChangeTest3() {
        User user = new User("a", null, "c", "d");

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc, times(1)).update(
                Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(user.getFullname()),
                Mockito.eq(user.getAbout()),
                Mockito.eq(user.getNickname()));

    }

    @Test
    @DisplayName("User gets list of users test #4")
    void ChangeTest4() {
        User user = new User("a", null, null, "d");

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc, times(1)).update(
                Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(user.getAbout()),
                Mockito.eq(user.getNickname()));
    }

    @Test
    @DisplayName("User gets list of users test #5")
    void ChangeTest5() {
        User user = new User("a", null, null, null);

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc, never()).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"));
    }
}
